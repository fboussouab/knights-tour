package be.bignited;

import be.bignited.views.sizeView.SizePresenter;
import be.bignited.views.sizeView.SizeView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainFx extends Application {
    @Override
    public void start(Stage stage) {
        SizeView sizeView = new SizeView();
        SizePresenter sizePresenter = new SizePresenter(sizeView);
        Scene scene = new Scene(sizeView);
        stage.setScene(scene);
        stage.setTitle("Size");
        stage.show();
    }
}
