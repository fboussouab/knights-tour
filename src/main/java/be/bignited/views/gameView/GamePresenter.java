package be.bignited.views.gameView;

import be.bignited.services.GameService;

public class GamePresenter {
    private GameService gameService;
    private GameView gameView;

    public GamePresenter(GameService gameService, GameView gameView) {
        this.gameService = gameService;
        this.gameView = gameView;
    }
}
