package be.bignited.views.sizeView;

import be.bignited.models.Grid;
import be.bignited.services.GameService;
import be.bignited.views.gameView.GamePresenter;
import be.bignited.views.gameView.GameView;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.util.logging.Logger;

public class SizePresenter {

    private static final Logger logger = Logger.getLogger(SizePresenter.class.getName());
    private SizeView sizeView;

    public SizePresenter(SizeView sizeView) {
        this.sizeView = sizeView;
        addEventHandlers();
    }

    private void addEventHandlers() {
        sizeView.getBtnOk().setOnAction(event -> {
            String sizeString = sizeView.getTfSize().getText();
            int size = 0;
            try {
                size = Integer.parseInt(sizeString);
            } catch (NumberFormatException e) {
                sizeView.getLblError().setText(String.format("%s in not a number, please enter an number!", sizeString));
            }
            if (size < 2 || size > 10) {
                new Alert(Alert.AlertType.ERROR, "Size must be between 2 and 10").showAndWait();
            } else {
                Grid grid = new Grid(size);
                GameService gameService = new GameService(grid);
                GameView gameView = new GameView();
                new GamePresenter(gameService, gameView);
                sizeView.getScene().setRoot(gameView);
                Stage stage = (Stage) gameView.getScene().getWindow();
                stage.setTitle("Game");
            }
        });
    }


}
