package be.bignited.views.sizeView;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class SizeView extends BorderPane {
    private TextField tfSize;
    private Button btnOk;

    private Label lblError;

    public SizeView() {
        tfSize = new TextField();
        btnOk = new Button("OK");
        lblError = new Label();
        tfSize.setFocusTraversable(false);
        tfSize.setPromptText("enter size");
        this.setTop(tfSize);
        this.setCenter(lblError);
        this.setBottom(btnOk);
        this.setPrefWidth(300);
        BorderPane.setAlignment(btnOk, Pos.BOTTOM_CENTER);
        BorderPane.setMargin(tfSize, new Insets(10));
        BorderPane.setMargin(btnOk, new Insets(10));
    }

    public TextField getTfSize() {
        return tfSize;
    }

    public Button getBtnOk() {
        return btnOk;
    }

    public Label getLblError() {
        return lblError;
    }
}
