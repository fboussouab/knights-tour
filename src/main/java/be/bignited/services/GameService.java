package be.bignited.services;

import be.bignited.models.Coordinates;
import be.bignited.models.Grid;

import java.util.ArrayList;
import java.util.List;

public class GameService {
    private final Grid grid;
    private Coordinates currentPosition;

    public GameService(Grid grid) {
        this.grid = grid;
    }

    public void Start() {
        this.grid.drawEmptyGrid();
    }

    public Grid getGrid() {
        return grid;
    }

    public int getSquareOnPosition(Coordinates coordinates) {
        currentPosition = new Coordinates(coordinates.row(), coordinates.col());
        return this.grid.getValueOnPosition(coordinates);
    }

    public void moveTo(Coordinates position) {
        this.currentPosition = position;
        this.grid.setPositionVisited(position);
        this.grid.drawGrid();
    }

    public List<Coordinates> getPossiblePositions() {
        int[][] moves = {
                {1, 2}, {2, 1},
                {-2, 1}, {-1, 2},
                {-2, -1}, {-1, -2},
                {1, -2}, {2, -1}
        };
        List<Coordinates> possiblePosition = new ArrayList<>();
        for (int[] move : moves) {
            Coordinates nextPossiblePosition = new Coordinates(currentPosition.row() + move[0], currentPosition.col() + move[1]);
            if (nextPossiblePosition.row() >= 0 && nextPossiblePosition.row() < this.grid.getSize()
                    && nextPossiblePosition.col() >= 0 && nextPossiblePosition.col() < this.grid.getSize()
                    && grid.getValueOnPosition(nextPossiblePosition) == 0)
                possiblePosition.add(nextPossiblePosition);
        }

        return possiblePosition;
    }
}
