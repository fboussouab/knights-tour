package be.bignited.models;

public record Coordinates(int row, int col){}
