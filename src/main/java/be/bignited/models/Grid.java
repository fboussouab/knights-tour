package be.bignited.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Grid {
    private final int[][] squares;
    private int counter;
    public Grid(int size) {
        this.squares = new int[size][size];
    }

    public void drawEmptyGrid() {
        initiateGrid();
        drawGrid();
    }

    public void drawGrid() {
        for (int[] row : squares) {
            for (int column : row) {
                System.out.printf("%3d", column);
            }
            System.out.println();
        }
    }

    private void initiateGrid() {
        for (int[] square : squares) {
            Arrays.fill(square, 0);
        }
    }

    public int[][] getSquares() {
        return squares;
    }

    public int getValueOnPosition(Coordinates coordinates) {
        return squares[coordinates.row()][coordinates.col()];
    }

    public void setPositionVisited(Coordinates coordinates) {
        squares[coordinates.row()][coordinates.col()] = ++ counter;
    }

    public int getSize() {
        return this.squares.length;
    }

    public List<Integer> getAllValues() {
        List<Integer> values = new ArrayList<>();
        for (int[] row : squares) {
            for (int value : row) {
                values.add(value);
            }
        }
        return values;
    }
}
