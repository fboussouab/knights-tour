package be.bignited;

import be.bignited.models.Coordinates;
import be.bignited.models.Grid;
import be.bignited.services.GameService;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class MainConsole {
    private static final Scanner scanner = new Scanner(System.in);
    private static GameService gameService;
    private static int size = 8;
    private static List<Coordinates> possiblePositions;
    private static final Logger logger = Logger.getLogger(MainConsole.class.getName());

    public static void main(String[] args) {
        setSize();
        gameService.Start();
        gameService.moveTo(choosePosition());
        do {
            possiblePositions = gameService.getPossiblePositions();
            if (possiblePositions.isEmpty()){
                System.out.println("=========================== GAME OVER ============================");
                if (gameService.getGrid().getAllValues().contains(0))
                {
                    System.out.println("=========================== YOU LOSE! ============================");
                } else {
                    System.out.println("=========================== YOU WIN! ============================");
                }
                break;
            }
            moveToNextPosition();
        } while (!possiblePositions.isEmpty());
    }

    private static void moveToNextPosition() {
        System.out.println("next Possible Positions: ");
        possiblePositions.forEach(position -> System.out.printf("- (%d, %d)%n", position.row(), position.col()));
        Coordinates nextPosition;
        do {
            System.out.println("Choose next position from list above: ");
            nextPosition = choosePosition();
        } while (!possiblePositions.contains(nextPosition));
        gameService.moveTo(nextPosition);
    }

    private static Coordinates choosePosition() {
        int startRow;
        int startColumn;
        do {
            System.out.printf("Enter row (between 0 and %d): ", size - 1);
            startRow = scanner.nextInt();
            System.out.printf("Enter column (between 0 and %d): ", size - 1);
            startColumn = scanner.nextInt();
        } while (startRow < 0 || startRow > size - 1 || startColumn < 0 || startColumn > size - 1);
        return new Coordinates(startRow, startColumn);
    }

    private static void setSize() {
        System.out.print("Enter the size: ");
        try {
            size = scanner.nextInt();
            Grid grid = new Grid(size);
            gameService = new GameService(grid);
        } catch (InputMismatchException e) {
            logger.severe("You entered a non numeric character, we will consider the default size = 8");
            System.exit(1);
        }

    }
}