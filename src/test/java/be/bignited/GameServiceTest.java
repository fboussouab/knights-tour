package be.bignited;

import be.bignited.models.Coordinates;
import be.bignited.models.Grid;
import be.bignited.services.GameService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameServiceTest {
    private GameService gameService;

    @BeforeEach
    public void setUp() {
        Grid grid = new Grid(8);
        gameService = new GameService(grid);
    }

    @Test
    public void startGame() {
        gameService.Start();
        int[][] zerosArray = {
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0},
        };
        assertArrayEquals(zerosArray, gameService.getGrid().getSquares());
    }

    @Test
    public void chooseStartPosition(){
        Coordinates coordinates = new Coordinates(4, 5);
        gameService.moveTo(coordinates);
        assertEquals(1, gameService.getSquareOnPosition(coordinates));
    }
}